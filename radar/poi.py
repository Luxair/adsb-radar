center_coord = (49.6332, 6.2670)

airport_coords = [('LUX-ELLX', (49.617212, 6.186466), (49.636122, 6.237195))]

cities = [
    ('Metz', (49.133333, 6.166667)),
    ('Nancy', (48.6921, 6.1844)),
    ('Verdun', (49.1599, 5.3844)),
    ('Paris', (48.8566, 2.3522)),
    ('Luxembourg', (49.6116, 6.1319)),
    ('Vigneulles-lès-Hattonchâtel', (48.9811, 5.7040)),
    ('Munsbach', (49.6332, 6.2670)),
    ('Brussels', (50.8503, 4.3517)),
    ('Trier', (49.7500, 6.6371)),
    ('Koln', (50.9375, 6.9603)),
    ('Frankfurt', (50.1109, 8.6821)),
    ('Strasbourg', (48.5734, 7.7521)),
    ('Eindhoven', (51.4416, 5.4697))
]

waypoints = {
    'REMBA': ('N50 39 44', 'E004 54 51', 3),
    'RITAX': ('N50 04 40', 'E005 48 25', 3),
    'OLNO': ('N50 35 09.3', 'E005 42 37.0', 6),
    'DIEKIRCH': ('N49 51 40.7', 'E006 07 47.1', 6),
    'LAKAL': ('N49 46 06', 'E006 04 25', 3),
    'CONDO': ('N49 46 12', 'E006 20 24', 3),
    'BETEX': ('N49 48 57', 'E006 25 31', 3),
    'PONIG': ('N49 45 36', 'E006 34 10', 3),
    'STINO': ('N49 37 30', 'E005 56 54', 3),
    'IRTON': ('N49 33 00', 'E005 33 00', 3),
    'PETAN': ('N49 33 10', 'E005 52 38', 3),
    'BREDI': ('N49 31 20', 'E006 17 30', 3),
    'MOSET': ('N49 32 47', 'E006 20 39', 3),
    'DUDAN': ('N49 28 42', 'E006 04 18', 3),
    'LUXEMBOURG': ('N49 38 22.3', 'E006 14 50.2', 6),
    'DISKI': ('N49 34 20', 'E006 28 14', 3),
    'FAP/FAF': ('N49 40 47', 'E006 21 19', 4),
    'WLU': ('N49 34 04.1', 'E006 03 14.7', 4)
}

routes = [
    ('LUXEMBOURG', 'DISKI'),
    ('LUXEMBOURG', 'DIEKIRCH'),
    ('DIEKIRCH', 'OLNO'),
    ('DIEKIRCH', 'BETEX'),
    ('DIEKIRCH', 'RITAX'),
    ('RITAX', 'REMBA'),
    ('PETAN', 'DIEKIRCH'),
    ('DIEKIRCH', 'MOSET'),
    ('FAP/FAF', 'LUXEMBOURG'),
    ('DIEKIRCH', 'WLU'),
    ('MOSET', 'WLU'),
    ('WLU', 'LUXEMBOURG'),
    ('BETEX', 'LUXEMBOURG')
]

radius = 1.00
