import logging
import subprocess
import time
from threading import Thread
from typing import Optional

import pyModeS as pms

import poi
from correction import correct
from custom_queue import NonBlockingQueue
from model import TrackingContext, AdsbMessageData, CATEGORY_IDT, CATEGORY_OPS, CATEGORY_TGT, CATEGORY_CRA, \
    CATEGORY_VEL, CATEGORY_POS, AdsbMessage

# Since ADSB v0
TC_IDENTIFICATION = [1, 2, 3, 4]
TC_POSITION = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
TC_VELOCITY = [19]
# Introduced in ADSB v1 and modified in v2
TC_AIRCRAFT_STATUS = [28]
TC_TARGET_STATE = [29]
TC_OPERATIONS_STATUS = [31]


class AdsbAccuracyMonitor(Thread):
    def __init__(self):
        Thread.__init__(self, name='AdsbAccuracyMonitor', daemon=True)

    def get_messages_per_second(self, ts: float) -> Optional[int]:
        pass

    def get_messages_per_second_as_string(self, ts: float) -> str:
        pass

    def get_error_rate(self) -> Optional[float]:
        pass

    def get_error_rate_as_string(self) -> str:
        pass

    def register_message(self, message: AdsbMessage, corrected_msg: AdsbMessage, errors: int):
        pass


class AdsbAccuracyLogger(AdsbAccuracyMonitor, Thread):
    def __init__(self):
        Thread.__init__(self, daemon=True, name='AdsbAccuracyLogger')
        AdsbAccuracyMonitor.__init__(self)

        self.error_counts = {0: 0, 1: 0, 2: 0, 3: 0, 99: 0}
        self.message_queue = NonBlockingQueue(capacity=1000,
                                              on_enter=self.__on_enter,
                                              on_leave=self.__on_leave)
        self.log = logging.getLogger('ADSB Accuracy')

    def __on_leave(self, queue_item):
        self.error_counts[queue_item[1]] -= 1

    def __on_enter(self, queue_item):
        self.error_counts[queue_item[1]] += 1

    def register_message(self, message: AdsbMessage, corrected_msg: str, errors: int):
        self.message_queue.put((message.timestamp, errors))

    def get_error_rate(self):
        count_error = 0
        count_total = 0

        for errcount, occurrences in self.error_counts.items():
            if errcount == 99:
                count_error += occurrences

            count_total += occurrences

        return None if count_total == 0 else count_error / count_total

    def get_error_rate_as_string(self):
        er = self.get_error_rate()
        return 'N/A' if er is None else '{0: >6}'.format('%.1f%%' % (100 * er))

    def get_messages_per_second(self, ts: float):
        oldest, messagecount = self.message_queue.peek()

        if oldest is None:
            return None

        old_ts, old_err = oldest

        mps = messagecount / (ts - old_ts)
        return mps

    def get_messages_per_second_as_string(self, ts: float):
        mps = self.get_messages_per_second(ts)
        return 'N/A' if mps is None else '{0: >5}'.format('%.1f' % mps)

    def run(self):
        while self.is_alive():
            time.sleep(5)

            self.log.info('%s msg/s, Err. rate: %s - %s',
                          self.get_messages_per_second_as_string(time.time()),
                          self.get_error_rate_as_string(),
                          self.error_counts)


class AdsbProcessor:

    def process_adsb(self, message: AdsbMessage) -> Optional[AdsbMessageData]:
        pass


class AdsbProducer:

    def __init__(self, processor: AdsbProcessor, **kwargs):
        self.next_processor = processor

    def interrupt(self):
        pass


class AdsbFilter(AdsbProcessor, AdsbProducer):

    def __init__(self,
                 processor: AdsbProcessor,
                 monitor: AdsbAccuracyMonitor = AdsbAccuracyMonitor(),
                 max_correction: int = 1):
        AdsbProducer.__init__(self, processor)
        AdsbProcessor.__init__(self)
        self.monitor = monitor
        self.max_correction = max_correction

    def process_adsb(self, message: AdsbMessage):
        corrected_msg, errors = correct(message, max_correction=self.max_correction)

        self.monitor.register_message(message, corrected_msg, errors)

        if corrected_msg is not None:
            self.next_processor.process_adsb(corrected_msg)


class AdsbReceiver(Thread, AdsbProducer):
    def __init__(self, processor: AdsbProcessor, identifier: str, lat: float, lon: float):
        self._interrupted = False
        self._identifier = identifier
        self._location = (lat, lon)
        Thread.__init__(self, name='AdsbReceiver')
        AdsbProducer.__init__(self, processor)

    def run(self):
        with subprocess.Popen(['rtl_adsb', '-g', '48'], stdout=subprocess.PIPE) as adsb_flow:
            while not self._interrupted:
                msg = adsb_flow.stdout.readline()

                if len(msg) == 0:
                    break

                message = AdsbMessage(timestamp=time.time(), message=msg.decode('ascii').strip('*;\r\n'),
                                      location=self._location, identifier=self._identifier)

                self.next_processor.process_adsb(message)

    def interrupt(self):
        self._interrupted = True


class AdsbParser(AdsbProcessor):

    def __init__(self, context: TrackingContext):
        self.context = context
        self.log = logging.getLogger('ADSB Data')

    def process_adsb(self, message: AdsbMessage) -> AdsbMessageData:
        msg = message.message
        tc = pms.adsb.typecode(msg)
        icao = pms.adsb.icao(msg)
        message_data = AdsbMessageData(tc=tc, icao=icao)

        aircraft = '-'
        altitude = '-'
        velocity = '-'

        self.context.detect_lost_aircrafts(message.timestamp)

        if tc in TC_IDENTIFICATION:
            message_data.category = CATEGORY_IDT
            callsign = pms.adsb.callsign(msg).strip('_')
            message_data.callsign = callsign
            aircraft = self.context.registerAircraft(message.timestamp, pms.adsb.icao(msg), callsign)
            aircraft.messages.append(message)
            aircraft.lastmessage = message.timestamp
            self.context.notify_update(message.timestamp, aircraft)

        if tc in TC_POSITION:
            message_data.category = CATEGORY_POS
            message_data.parity = pms.adsb.oe_flag(msg)
            altitude = pms.adsb.altitude(msg)
            aircraft = self.context.getAircraft(message.timestamp, icao)

            if aircraft is not None:
                aircraft.messages.append(message)
                aircraft.lastmessage = message.timestamp
                aircraft.altitude = altitude
                message_data.altitude = altitude
                lat, lon = pms.adsb.position_with_ref(msg, poi.center_coord[0], poi.center_coord[1])
                aircraft.lat = lat
                aircraft.lon = lon

                message_data.lat = lat
                message_data.lon = lon

                self.context.notify_update(message.timestamp, aircraft)

        if tc in TC_VELOCITY:
            message_data.category = CATEGORY_VEL
            velocity = pms.adsb.velocity(msg)
            aircraft = self.context.getAircraft(message.timestamp, icao)

            if aircraft is not None and velocity is not None:
                aircraft.messages.append(message)
                aircraft.lastmessage = message.timestamp
                aircraft.speed, aircraft.heading, aircraft.vspeed, aircraft.sptype = velocity
                message_data.speed, message_data.heading, message_data.vspeed, message_data.sptype = velocity

                self.context.notify_update(message.timestamp, aircraft)

        if tc in TC_AIRCRAFT_STATUS:
            message_data.category = CATEGORY_CRA

        if tc in TC_TARGET_STATE:
            message_data.category = CATEGORY_TGT

        if tc in TC_OPERATIONS_STATUS:
            message_data.category = CATEGORY_OPS
            version = pms.adsb.version(msg)
            message_data.version = version

        self.log.debug(
            '%s %2d %s %s %s %s',
            msg, tc, icao,
            velocity, altitude,
            aircraft
        )

        return message_data
