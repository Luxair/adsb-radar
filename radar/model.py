import logging
import time
from dataclasses import dataclass
from math import sqrt
from typing import List, Optional, Tuple

import orjson

CATEGORY_OPS = 'OPS'
CATEGORY_TGT = 'TGT'
CATEGORY_CRA = 'CRA'
CATEGORY_VEL = 'VEL'
CATEGORY_POS = 'POS'
CATEGORY_IDT = 'IDT'

NO_CALLSIGN = '???'


@dataclass
class AdsbMessage:
    timestamp: float
    message: str
    identifier: str = None
    location: Tuple[float, float] = (0, 0)

    def to_json(self) -> bytes:
        return orjson.dumps(self)


def from_json(text: bytes) -> AdsbMessage:
    message_dict = orjson.loads(text)
    message_object = AdsbMessage(**message_dict)

    # Location is deserialized as list which is not compliant with dataclass definition
    message_object.location = tuple(message_object.location)

    return message_object


@dataclass
class AdsbMessageData:
    tc: int
    icao: str
    category: Optional[str] = None
    parity: Optional[int] = None
    callsign: Optional[str] = None
    lat: Optional[float] = None
    lon: Optional[float] = None
    altitude: Optional[float] = None
    heading: Optional[float] = None
    speed: Optional[float] = None
    vspeed: Optional[float] = None
    sptype: Optional[str] = None
    version: Optional[int] = None


class AircraftPosition:
    def __init__(self, ts: float, lat: float, lon: float, alt: float):
        self.ts = ts
        self.lat = lat
        self.lon = lon
        self.alt = alt

    def distance(self, other_position) -> float:
        return sqrt(pow(self.lon - other_position.lon, 2) + pow(self.lat - other_position.lat, 2))

    def __str__(self):
        return 'lat=%f, lon=%f, alt=%f' % (self.lat, self.lon, self.alt)


class AircraftTrajectory:
    def __init__(self):
        self.positions = []

    def register_position(self, ts: float, lat: float, lon: float, alt: float) -> None:
        if lat is not None and lon is not None and alt is not None:
            if self.positions != [] and self.positions[-1].lat == lat and self.positions[-1].lon == lon:
                self.positions[-1].alt = alt
            else:
                position = AircraftPosition(ts, lat, lon, alt)
                self.positions = self.positions + [position]


class Aircraft:
    def __init__(self, ts, icao, callsign):
        self.icao = icao
        self.callsign = callsign
        self.messages: List[AdsbMessage] = []
        self.trajectory = AircraftTrajectory()
        self.firstmessage = ts
        self.lastmessage = ts
        self.altitude = None
        self.lat = None
        self.lon = None
        self.speed = None
        self.heading = None
        self.vspeed = None
        self.sptype = None

    def distance(self, lat, lon):
        if self.lat is not None and self.lon is not None:
            return sqrt(pow(lat - self.lat, 2) + pow(lon - self.lon, 2))
        else:
            return 99

    def getLatency(self, ts: float) -> float:
        return ts - self.lastmessage

    def getAverageInterval(self) -> float:
        msglen = len(self.messages)
        return None if msglen < 2 else \
            (self.lastmessage - self.firstmessage) / (msglen - 1)

    def getTrackingStatus(self) -> bool:
        ts = time.time()
        avg = self.getAverageInterval()
        lat = self.getLatency(ts)

        if avg is None:
            return lat < 120
        else:
            return lat < 10 * avg or lat < 10

    def isLost(self, ts: float):
        return self.getLatency(ts) > 600

    def getJsonData(self, ts: float):
        return {
            'icao': self.icao,
            'tracked': self.getTrackingStatus(),
            'lost': self.isLost(ts),
            'callsign': self.callsign,
            'lat': self.lat,
            'lon': self.lon,
            'altitude': self.altitude
        }

    def __str__(self):
        return 'Aircraft{icao=%s, callsign=%-8s, lat=%s, lon=%s}' % (
            self.icao, self.callsign, str(self.lat), str(self.lon))


class TrackingObserver:

    def aircraft_updated(self, aircraft: Aircraft):
        pass

    def aircraft_lost(self, aircraft: Aircraft):
        pass


class TrackingContext:
    def __init__(self, observers: List[TrackingObserver] = None):
        if observers is None:
            observers = []

        self.aircrafts: List[Aircraft] = []
        self.observers = list(observers)
        self.log = logging.getLogger('TrackingContext')

    def addObserver(self, obs: TrackingObserver):
        self.observers.append(obs)

    def registerAircraft(self, ts: float, icao: str, callsign: str) -> Aircraft:
        result = self.getAircraft(ts, icao)
        result.callsign = callsign
        return result

    def getAircraft(self, ts: float, icao: str) -> Aircraft:
        result = None

        for aircraft in self.aircrafts:
            if aircraft.icao == icao:
                result = aircraft

        if result is None:
            result = Aircraft(ts, icao, NO_CALLSIGN)
            self.aircrafts = [result] + self.aircrafts

        return result

    def detect_lost_aircrafts(self, ts: float):
        lost_crafts = [ac for ac in self.aircrafts if ac.isLost(ts)]

        for ac in lost_crafts:
            self.notify_lost(ac)

        self.aircrafts = [ac for ac in self.aircrafts if ac not in lost_crafts]

    def notify_update(self, ts: float, aircraft: Aircraft):
        aircraft.trajectory.register_position(ts, aircraft.lat, aircraft.lon, aircraft.altitude)

        if len(aircraft.trajectory.positions) >= 2 and \
            aircraft.trajectory.positions[-1].distance(aircraft.trajectory.positions[-2]) > 1:
            self.log.warning('Removing suspicious position for aircraft ' + aircraft.callsign
                             + ' from ' + str(aircraft.trajectory.positions[-2])
                             + ' to ' + str(aircraft.trajectory.positions[-1]))

            aircraft.trajectory.positions = aircraft.trajectory.positions[:-1]

        for obs in self.observers:
            obs.aircraft_updated(aircraft)

    def notify_lost(self, aircraft: Aircraft):
        for obs in self.observers:
            obs.aircraft_lost(aircraft)
