def hms_to_degrees(hms: str) -> float:
    h, m, s = hms[1:].split()

    h = float(h)
    m = float(m)
    s = float(s)

    if hms[0] in ['S', 'W']:
        h = -h
        m = -m
        s = -s

    return h + m / 60 + s / 3600


class Projection:

    def __init__(self, screen_w: float, screen_h: float, gps_center_lat: float, gps_center_lon: float, radius: float):
        self.screen_w = screen_w
        self.screen_h = screen_h
        self.gps_center_lat = gps_center_lat
        self.gps_center_lon = gps_center_lon
        self.radius = radius

        self.px_per_deg = self.screen_h / (2 * self.radius)
        self.cx = self.screen_w / 2
        self.cy = self.screen_h / 2

    def get_screen_coordinates(self, lat: float, lon: float):
        rel_lat = lat - self.gps_center_lat
        rel_lon = lon - self.gps_center_lon
        x = self.cx + rel_lon * self.px_per_deg
        y = self.cy - rel_lat * self.px_per_deg
        return x, y

    def get_screen_coordinates_60(self, lat: str, lon: str):
        return self.get_screen_coordinates(hms_to_degrees(lat), hms_to_degrees(lon))

    def get_pixels(self, degrees: float):
        return degrees * self.px_per_deg

    def get_grid_steps_x(self, precision):
        min_lon = self.gps_center_lon - self.cx / self.px_per_deg
        max_lon = self.gps_center_lon + self.cx / self.px_per_deg

        steps = []
        l = round(min_lon, precision)
        step = 1 / pow(10, precision)

        while l < max_lon:
            steps.append((round(l, precision), self.get_screen_coordinates(0, l)[0]))
            l += step

        return steps

    def get_grid_steps_y(self, precision):
        min_lat = self.gps_center_lat - self.cy / self.px_per_deg
        max_lat = self.gps_center_lat + self.cy / self.px_per_deg

        steps = []
        l = round(min_lat, precision)
        step = 1 / pow(10, precision)

        while l < max_lat:
            steps.append((round(l, precision), self.get_screen_coordinates(l, 0)[1]))
            l += step

        return steps
