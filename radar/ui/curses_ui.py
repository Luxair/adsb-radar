import curses
import time
from typing import Tuple

from adsb import AdsbAccuracyMonitor
from model import TrackingObserver, TrackingContext, Aircraft

LINE_ART = '---------------------------------------------------------------'


class GlobalsRenderer:

    def render(self, tracking_context: TrackingContext, adsb_monitor: AdsbAccuracyMonitor):
        pad = curses.newpad(3, 80)
        ts = time.time()

        pad.addstr(0, 3, 'Messages per second: %s' % adsb_monitor.get_messages_per_second_as_string(ts))
        pad.addstr(1, 3, '         Error Rate: %s' % adsb_monitor.get_error_rate_as_string())
        pad.addstr(2, 3, '             Crafts: %3d' % len(tracking_context.aircrafts))
        return pad


class AircraftsRenderer:

    def render(self, tracking_context: TrackingContext, center: Tuple[float, float]):
        aircrafts = sorted(tracking_context.aircrafts, key=lambda ac: ac.distance(center[0], center[1]))
        pad = curses.newpad(4 * len(aircrafts) + 1, 80)

        pad.addstr(0, 0, LINE_ART)

        for i, ac in enumerate(aircrafts):
            # Note: Style is not yet applied to outputs
            style = curses.COLOR_MAGENTA
            status = '💚' if ac.getTrackingStatus() else '🖤'

            if ac.altitude is not None and ac.vspeed is not None:
                if ac.altitude < 10000 and ac.vspeed > 100:
                    style = curses.COLOR_CYAN
                    status += '🛫'
                elif ac.altitude < 10000 and ac.vspeed < -100:
                    style = curses.COLOR_YELLOW
                    status += '🛬'
            else:
                status += ' '

            row = 1 + 3 * i
            pad.addstr(row + 0, 0,
                       ' %s   %s / %-8s   Dist: %3.2f' % (
                           status, ac.icao, ac.callsign, ac.distance(center[0], center[1])), style
                       )
            pad.addstr(row + 1, 0,
                       '       Alt: %6s    HSpeed: %6s    VSpeed: %06s' % (
                           str(ac.altitude), str(ac.speed), str(ac.vspeed)), style
                       )
            pad.addstr(row + 2, 0, LINE_ART)

        return pad


class CursesUI(TrackingObserver):

    def __init__(self, tracking_context: TrackingContext, adsb_monitor: AdsbAccuracyMonitor,
                 center: Tuple[float, float]):
        self.__tracking_context = tracking_context
        self.__tracking_context.addObserver(self)
        self.__adsb_monitor = adsb_monitor
        self.__center = center

    def aircraft_updated(self, aircraft: Aircraft):
        pass

    def aircraft_lost(self, aircraft: Aircraft):
        pass

    def run(self):
        curses.wrapper(self.main)

    def main(self, stdscr):
        # win = curses.newwin(curses.LINES, curses.COLS, 0, 0)

        globals_renderer = GlobalsRenderer()
        aircrafts_renderer = AircraftsRenderer()

        curses.curs_set(0)

        while True:
            stdscr.clear()

            pad_globals = globals_renderer.render(tracking_context=self.__tracking_context,
                                                  adsb_monitor=self.__adsb_monitor)
            pad_aircrafts = aircrafts_renderer.render(tracking_context=self.__tracking_context, center=self.__center)

            pad_globals.refresh(0, 0, 0, 0, 3, 40)
            pad_aircrafts.refresh(0, 0, 3, 0, curses.LINES - 1, curses.COLS - 1)

            time.sleep(1)
