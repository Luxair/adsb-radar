import time
from unittest import TestCase

from adsb import AdsbParser
from model import TrackingContext, AdsbMessageData, AdsbMessage


class TestAdsbParsing(TestCase):
    messages = [
        ('8daad012589724fb3560de5f263d',
         AdsbMessageData(tc=11, icao='aad012', category='POS', parity=1, lat=50.3104, lon=6.70567, altitude=29050)),
        ('8daad012e10d3400000000a14edb',
         AdsbMessageData(tc=28, icao='aad012', category='CRA')),
        ('8daad0129911ed93386c0168f66c',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.17, speed=514, vspeed=-1664, sptype='GS')),
        ('8daad0129911ee93586c81a56b85',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1664, sptype='GS')),
        ('8daad0129911ee93586c01a26d45',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1664, sptype='GS')),
        ('8daad012f8210002004ab863bec5',
         AdsbMessageData(tc=31, icao='aad012', category='OPS', version=2)),
        ('8daad0125895f18a076add3656b6',
         AdsbMessageData(tc=11, icao='aad012', category='POS', lat=50.30873, lon=6.71416, altitude=28975, parity=0)),
        ('8daad0125895e4fadd6169659b18',
         AdsbMessageData(tc=11, icao='aad012', category='POS', lat=50.30835, lon=6.71599, altitude=28950, parity=1)),
        ('8daad012ea15985d315c08082117',
         AdsbMessageData(tc=29, icao='aad012', category='TGT')),
        ('8daad0129911ee93586c81a56b85',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1664, sptype='GS')),
        ('8daad0129911ee935870810de985',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1728, sptype='GS')),
        ('8daad0129911ee93587002f50757',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1728, sptype='GS')),
        ('8daad012f8210002004ab863bec5',
         AdsbMessageData(tc=31, icao='aad012', category='OPS', version=2)),
        ('8daad0125895b189a76b76d0c8c9',
         AdsbMessageData(tc=11, icao='aad012', category='POS', lat=50.30653, lon=6.72522, altitude=28875, parity=0)),
        ('8daad0129911ee935870010aef45',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1728, sptype='GS')),
        ('8daad0129911ee93587002f50757',
         AdsbMessageData(tc=19, icao='aad012', category='VEL', heading=107.24, speed=516, vspeed=-1728, sptype='GS')),
        ('8daad012f8210002004ab863bec5',
         AdsbMessageData(tc=31, icao='aad012', category='OPS', version=2)),
        ('8daad012ea15985d315c08082117',
         AdsbMessageData(tc=29, icao='aad012', category='TGT')),
        ('8daad012589581894b6c092a6cf3',
         AdsbMessageData(tc=11, icao='aad012', category='POS', lat=50.30443, lon=6.73585, altitude=28800, parity=0)),
    ]

    def test_message_data_extraction(self):
        tracking_context = TrackingContext()
        adsb_parser = AdsbParser(tracking_context)

        for message, expected_data in TestAdsbParsing.messages:
            mo = AdsbMessage(timestamp=time.time(), message=message)
            actual_data = adsb_parser.process_adsb(mo)
            # print("('%s', %s)," % (message, data))
            self.assertEqual(expected_data, actual_data)
