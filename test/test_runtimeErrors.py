import unittest
from unittest import TestCase
from unittest.mock import patch

from adsb import AdsbParser
from model import TrackingContext, AdsbMessage


class TestRuntimeErrors(TestCase):

    @patch('model.TrackingObserver')
    def test_with_actual_results(self, observerMock):
        context = TrackingContext()
        context.addObserver(observerMock)
        adsb_thread = AdsbParser(context)

        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e688280439fc582d'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b9813105596a853a7b'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a2058c1f8d74de0774740'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e68828043803ac24'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b984a2874d159c105b'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e78828043800a6ea'))
        adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b981301555e63c97a2'))

        self.assertEqual(len(context.aircrafts), 1)
        self.assertTrue(context.aircrafts[0].vspeed is not None)

        observerMock.aircraft_updated.assert_called_with(context.aircrafts[0])

    @patch('model.TrackingObserver')
    def test_with_empty_results(self, observerMock):
        context = TrackingContext()
        context.addObserver(observerMock)
        adsb_thread = AdsbParser(context)

        with unittest.mock.patch('pyModeS.adsb.velocity', return_value=None):
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e688280439fc582d'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b9813105596a853a7b'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a2058c1f8d74de0774740'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e68828043803ac24'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b984a2874d159c105b'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a9945e78828043800a6ea'))
            adsb_thread.process_adsb(AdsbMessage(0, '8d34409a58b981301555e63c97a2'))

        self.assertEqual(len(context.aircrafts), 1)
        self.assertTrue(context.aircrafts[0].vspeed is None)

        observerMock.aircraft_updated.assert_called_with(context.aircrafts[0])
