from unittest import TestCase

from custom_queue import NonBlockingQueue


class TestQueue(TestCase):
    def test_basic_behavior(self):
        q = NonBlockingQueue(3)

        self.assertEqual(q.peek(), (None, 0))

        q.put('a')
        self.assertEqual(q.peek(), ('a', 1))
        self.assertEqual(q.peek(), ('a', 1))

        q.put('b')
        self.assertEqual(q.peek(), ('a', 2))
        self.assertEqual(q.peek(), ('a', 2))

        q.put('c')
        self.assertEqual(q.peek(), ('a', 3))
        self.assertEqual(q.peek(), ('a', 3))

        q.put('d')
        self.assertEqual(q.peek(), ('b', 3))
        self.assertEqual(q.peek(), ('b', 3))

    def test_callbacks(self):
        entries = []
        exits = []

        q = NonBlockingQueue(3,
                             on_enter=entries.append,
                             on_leave=exits.append)

        self.assertEqual([], entries)
        self.assertEqual([], exits)
        q.put('a')
        self.assertEqual(['a'], entries)
        self.assertEqual([], exits)
        q.put('b')
        self.assertEqual(['a', 'b'], entries)
        self.assertEqual([], exits)
        q.put('c')
        self.assertEqual(['a', 'b', 'c'], entries)
        self.assertEqual([], exits)
        q.put('d')
        self.assertEqual(['a', 'b', 'c', 'd'], entries)
        self.assertEqual(['a'], exits)
