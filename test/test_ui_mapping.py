from unittest import TestCase

from ui.map import hms_to_degrees


class TestUIMapping(TestCase):

    def test_hms_to_degrees(self):
        self.assertAlmostEqual(49.861305555, hms_to_degrees('N49 51 40.7'), places=5)
        self.assertAlmostEqual(6.12975, hms_to_degrees('E006 07 47.1'), places=5)
        self.assertAlmostEqual(-6.12975, hms_to_degrees('W006 07 47.1'), places=5)
        self.assertAlmostEqual(-49.861305555, hms_to_degrees('S49 51 40.7'), places=5)
