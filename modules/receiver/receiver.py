import logging
from argparse import ArgumentParser

from adsb import AdsbReceiver
from messaging import RedisQueuePublisher

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    parser = ArgumentParser(description='An ADSB message receiver.')
    parser.add_argument('host', help='Redis host')
    parser.add_argument('list', help='Redis list object for upload')
    parser.add_argument('identifier', help='Receiver identifier')
    parser.add_argument('lat', help='Receiver latitude')
    parser.add_argument('lon', help='Receiver longitude')

    ns = parser.parse_args()

    adsb_publisher = RedisQueuePublisher(host=ns.host, listname=ns.list)
    adsb_receiver = AdsbReceiver(adsb_publisher, ns.identifier, float(ns.lat), float(ns.lon))
    adsb_receiver.start()
