import logging
import sys

from adsb import AdsbParser
from cache import CacheManager
from messaging import RedisTopicConsumer
from model import TrackingContext

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    contextObserver = CacheManager(host=sys.argv[3], key=sys.argv[4])
    # contextObserver.log.setLevel(logging.DEBUG)

    context = TrackingContext(observers=[contextObserver])
    parser = AdsbParser(context)
    consumer = RedisTopicConsumer(host=sys.argv[1], channel=sys.argv[2], processor=parser)

    consumer.start()
