FROM python:3.9-buster

RUN apt-get update && apt-get install -y rtl-sdr
RUN pip install --upgrade redis psycopg2 pip

COPY requirements.txt /
RUN pip install -r /requirements.txt

RUN mkdir -p /opt/radar
WORKDIR /opt/radar

COPY radar/* /opt/radar/

